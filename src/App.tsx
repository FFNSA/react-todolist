import React, {useState} from 'react';
import './App.css';

interface ToDoElement {
    id: number;
    text: string;
}

function App() {

    const [deleteList, setDeleteList] = useState<Set<number>>(new Set());
    const [dragging, setDragging] = useState<number>();
    const [editingIndex, setEditingIndex] = useState<number | undefined>();
    const [multipleDelete, setMultipleDelete] = useState<boolean>(false);
    const [toDoList, setToDoList] = useState<ToDoElement[]>([]);

    /**
     * Add a new element to the to-do list
     * @param e
     */
    function addElement(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        // Save form data in a proper way
        const data = new FormData(e.currentTarget);

        // Check user's input validity
        if (data.get('new-element')?.toString().trim().length) {
            // Update state and view
            setToDoList(toDoList.concat({id: Date.now(), text: data.get('new-element')?.toString().trim() || ''}));
        } else {
            // Display a warning and do not add any element to the list
            alert('Please insert a valid item in the list');
        }

        // Reset form
        e.currentTarget.reset();
    }

    /**
     * Remove an element from the to-do list
     * @param e
     */
    function removeElement(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        setToDoList(toDoList.slice().filter((v, i) => i !== Number(e.currentTarget.dataset.index)));
    }

    /**
     * Edit the specified element of the to-do list
     * @param e
     */
    function editElement(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();

        // Save a copy of the to-do list
        const updatedToList = toDoList.slice();

        // Save form data in a proper way
        const data = new FormData(e.currentTarget);

        // Update text of the element at the clicked index
        updatedToList[Number(editingIndex)].text = data.get('edit-element')?.toString() || '';

        // Update state and view
        setToDoList(updatedToList);

        // Reset "edit" toggle
        setEditingIndex(undefined);
    }

    /**
     * Clear the to-do list (i.e., set it to an empty array)
     * @param e
     */
    function clearList(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        if (window.confirm('Confirm operation?')) {
            setToDoList([]);
        }
    }

    /**
     * Enable/disable the "edit mode", where an element's text gets to be edited by user
     * @param e
     */
    function toggleEditMode(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        // Edit is disallowed when a multiple deletion or another edit is already in progress
        if (!multipleDelete && (e.type !== 'dblclick' || (e.type === 'dblclick' && editingIndex === undefined))) {
            // Save index of the element to be edited (or delete it if already set)
            setEditingIndex(editingIndex !== undefined ? undefined : Number(e.currentTarget.dataset.index));
        }
    }

    /**
     * Enable/disable the "multiple deletion mode", where multiple elements can be deleted at once
     * @param e
     */
    function toggleMultipleDelete(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        // Reset the list if the toggle is being set to off
        if (multipleDelete) {
            setDeleteList(new Set());
        }

        setMultipleDelete(!multipleDelete);
    }

    /**
     * Update the auxiliary list, used to keep track on the elements to be deleted, if the user so chooses
     * @param e
     */
    function updateDeleteList(e: React.ChangeEvent<HTMLInputElement>) {
        const newSet = new Set(deleteList);

        // Add an element to auxiliary list if the complementary input is checked. Remove it otherwise
        if (e.currentTarget.checked) {
            newSet.add(Number(e.currentTarget.dataset.deleteIndex));
        } else {
            newSet.delete(Number(e.currentTarget.dataset.deleteIndex));
        }

        setDeleteList(newSet);
    }

    /**
     * Delete the previously selected elements from the to-do list
     * @param e
     */
    function execMultipleDelete(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();

        // Make a copy of the current to-do list
        const toDoListCopy = toDoList.slice();

        // Cast Set values to Array to use the methods of the Array class
        const deleteArray = Array.from(deleteList.values());

        // Update the to-list
        setToDoList(toDoListCopy.filter((v, i) => deleteArray.indexOf(i) === -1));

        // Reset the delete list
        setDeleteList(new Set());

        // Disable the multiple delete mode
        setMultipleDelete(false);
    }

    /**
     * Save the index of the element currently being dragged
     * @param e
     */
    function onDragging(e: React.DragEvent<HTMLLIElement>) {
        setDragging(Number(e.currentTarget.dataset.index));
    }

    /**
     * Swap the element currently being dragged with the one currently being dragged over
     * @param e
     */
    function onDraggingOver(e: React.DragEvent<HTMLLIElement>) {
        // Preventing default allows to capture the "drop" event
        e.preventDefault();

        // Reorder the array only if the dragging index isn't equal to its own index,
        // because the "dragover" event is fired even on itself.
        // Also don't reorder the array when a multiple deletion or an edit is in progress
        if (dragging !== undefined && dragging !== Number(e.currentTarget.dataset.index) && !multipleDelete && editingIndex === undefined) {
            // Make a copy of the current to-do list
            const toDoListCopy = toDoList.slice();

            // Swap the elements
            const removed = toDoListCopy.splice(dragging, 1)[0];
            toDoListCopy.splice(Number(e.currentTarget.dataset.index), 0, removed);

            // Update state and view
            setToDoList(toDoListCopy);
        }
    }

    /**
     * "Render" function
     */
    return (
        <div style={{marginLeft: '15px', marginRight: '15px'}}>
            <h1>TO DO LIST</h1>
            <h4>Elements are sortable. Also they're editable even double-clicking on them</h4>

            {/*Form used to add elements to the list*/}
            <form onSubmit={addElement} style={{display: 'inline-block'}}>
                <input required disabled={editingIndex !== undefined || multipleDelete} type="text" name="new-element"
                       id="new-element"
                       placeholder="Insert element text"/>
                <button type="submit" style={{marginLeft: '15px'}}
                        disabled={editingIndex !== undefined || multipleDelete}>➕ Add Element
                </button>
            </form>

            {/*Button used to clear the list (i.e. set it to empty array)*/}
            <button type="button" style={{display: 'inline-block', marginLeft: '15px'}}
                    onClick={clearList} disabled={editingIndex !== undefined || !toDoList.length || multipleDelete}>
                🚫 Clear List
            </button>

            {/*Multiple deletion feature*/}
            {
                !multipleDelete
                    ? <button style={{marginLeft: '15px'}} disabled={!toDoList.length || editingIndex !== undefined}
                              onClick={toggleMultipleDelete}>Multiple 🗑️
                    </button>
                    : <>
                        <button style={{marginLeft: '15px'}} disabled={!toDoList.length || editingIndex !== undefined}
                                onClick={execMultipleDelete}>✔️
                        </button>
                        <button style={{marginLeft: '10px'}} disabled={!toDoList.length || editingIndex !== undefined}
                                onClick={toggleMultipleDelete}>❌
                        </button>
                    </>
            }

            {/*Actual to-do list*/}
            <ul>
                {
                    toDoList.map((e, i) =>
                        <li key={e.id.toString()}
                            style={{
                                listStyle: multipleDelete ? 'none' : 'disc',
                                marginBottom: '5px',
                                marginLeft: multipleDelete ? '-20px' : '',
                            }} draggable={!multipleDelete && editingIndex === undefined}
                            data-index={i}
                            onDrag={onDragging}
                            onDragOver={onDraggingOver}>
                            <div style={{display: "flex", flexDirection: "row"}}>
                                {
                                    editingIndex !== undefined && i === editingIndex
                                        // Display a form on the row of the currently edited element (if any)
                                        ? <>
                                            <form style={{display: 'flex'}} onSubmit={editElement}>
                                                <input required type="text" name="edit-element" id="edit-element"
                                                       defaultValue={e.text}/>
                                                <button type="submit" style={{marginLeft: '15px'}}>✔️</button>
                                            </form>
                                            <button onClick={toggleEditMode} style={{marginLeft: '10px'}} data-index={i}>❌
                                            </button>
                                        </>
                                        // Otherwise display the element as simple text (plus CRUD buttons)
                                        : <>
                                            {
                                                multipleDelete &&
                                                <input type={"checkbox"} data-delete-index={i} name={`multiple_delete_${i}`}
                                                       onChange={updateDeleteList}/>
                                            }
                                            <span style={{
                                                cursor: !multipleDelete && editingIndex === undefined ? 'row-resize' : 'auto',
                                                width: '35%'
                                            }} onDoubleClick={toggleEditMode}
                                                  data-index={i}>{e.text}</span>
                                            {/*Display CRUD buttons only if the multiple delete toggle is set to false*/}
                                            {
                                                !multipleDelete && <div>
                                                    <button onClick={toggleEditMode} style={{marginLeft: '10px'}}
                                                            data-index={i} disabled={editingIndex !== undefined}>✏️
                                                    </button>
                                                    <button onClick={removeElement} style={{marginLeft: '10px'}}
                                                            data-index={i} disabled={editingIndex !== undefined}>🗑️
                                                    </button>
                                                </div>
                                            }
                                        </>
                                }
                            </div>
                        </li>
                    )
                }
            </ul>
        </div>
    );
}

export default App;
