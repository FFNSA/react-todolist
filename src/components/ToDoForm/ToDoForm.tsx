import {FormEvent} from "react";


function ToDoForm() {

    function onSubmit(e: FormEvent<HTMLFormElement>) {
        e.preventDefault();
    }

    function addElement() {

    }
    
    return (
        <form onSubmit={onSubmit}>
            <input type="text" name="todo-element"/>
            <button onClick={addElement} type="submit">Add Element</button>
        </form>
    );
}

export default ToDoForm;
